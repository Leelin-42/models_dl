# %% [markdown]
# #  CVAE（Condiational Variational AutoEncoder）
# 本次作业实现CVAE,数据集依然采用Fashion-Mnist。
# 本次作业的主要内容如下：
# - 深入学习CVAE（Condiational Variational AutoEncoder），如果你对该部分不熟悉，那么你的学习路线应该是AE->VAE->CVAE，关于这部分，网上有很多参考资料，可以充分结合网上的和课堂上老师所讲内容进行学习
# - 为了简化难度，本次作业给大家提供了一份参考代码，因此大家只需要进行一些代码补全即可。
#
# 注：
# 1.该框架的任何参数都可以被修改，你可以尝试不同的参数以达到更好的效果。
# 2. VAE有非常多的变种，在查资料的时候请注意斟酌。
# 3.**截止日期：2022/05/17,22:00**

# %%
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
import numpy as np
import os
import struct
from matplotlib import pyplot as plt

from tqdm import tqdm
from random import randint


# %%
def settle_seed(seed=42):

    ###########################################
    # 你需要固定住随机数种子使得你的代码可复现(5/100)
    # TODO: Implement Here
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    np.random.seed(seed)
    torch.backends.cudnn.deterministic = True
    ############################################


settle_seed()


# %%
# data preprocess
def open_image(data_path):
    buffer = open(data_path, 'rb').read()
    index = 0
    magic, numImages, numRows, numColumns = struct.unpack_from(
        '>IIII', buffer, index)
    assert numRows == 28
    index += struct.calcsize('>IIII')
    data = []
    for i in range(numImages):
        im = struct.unpack_from('>784B', buffer, index)
        index += struct.calcsize('>784B')
        im = np.array(im).reshape(numRows, numColumns)
        data.append(im)
        # img = Image.fromarray(im.astype('uint8')).convert('RGB')
        # img.show()
    return data


def open_label(label_path):
    buffer = open(label_path, 'rb').read()
    index = 0
    magic, numLabels = struct.unpack_from('>II', buffer, index)
    index += struct.calcsize('>II')
    label = []
    for i in range(numLabels):
        lab = struct.unpack_from('>B', buffer, index)
        index += struct.calcsize('>B')  #
        label.append(lab[0])
    return label


# %%
class FashionDataset(Dataset):

    def __init__(self, data_path, label_path):
        self.train_data = open_image(data_path)
        self.train_label = open_label(label_path)

    def __getitem__(self, item):
        return torch.tensor(self.train_data[item],
                            dtype=torch.float), torch.tensor(
                                self.train_label[item])

    def __len__(self):
        return len(self.train_data)


# %%
class MLPEncoder(nn.Module):
    """
        Treat as VAE Encoder, to estimate the posterior distribution of z P(z|x, y)
    """

    def __init__(self, hidden_size=200, latent_size=2):
        super(MLPEncoder, self).__init__()
        self.m1 = nn.Linear(784 + 10, hidden_size)  # 784 + 10
        self.m2 = nn.Linear(hidden_size, latent_size)

        self.s1 = nn.Linear(784 + 10, hidden_size)
        self.s2 = nn.Linear(hidden_size, latent_size)
        self.relu = nn.ReLU()

    def forward(self, x, c):
        # 实现Encoder的前向计算，返回隐变量的后验分布的均值和标准差(10/100)
        # P(z|x, y), as Inference network

        mean, std = None, None
        ###############################################
        # TODO: Implement Here
        # print(x.size(), c.size())
        x = torch.cat((x, c), dim=-1)
        mean = self.m1(x)
        mean = self.m2(mean)
        std = self.s1(x)
        std = torch.abs(self.s2(std))
        # std = self.relu(std) + 1e-7
        ###############################################

        return mean, std


# %%
class ConditionEncoder(nn.Module):
    """
        Estimate the posterior distribution of z P(z|x), we
        treat x as the label of MNIST
    """

    def __init__(self, hidden_size=200, latent_size=2):
        super(ConditionEncoder, self).__init__()
        self.fc1 = nn.Linear(10, hidden_size)
        self.std_fc = nn.Linear(hidden_size, latent_size)
        self.mean_fc = nn.Linear(hidden_size, latent_size)

    def forward(self, c):
        #  实现编码条件的Encoder的前向计算，返回隐变量的先验分布的均值和标准差(10/100)
        mean, std = None, None
        ###############################################
        # TODO: Implement Here
        tmp = self.fc1(c)
        mean = self.mean_fc(tmp)
        std = torch.abs(self.std_fc(tmp))
        # std = self.relu(std) + 1e-7
        ###############################################
        return mean, std


# %%
class MLPDecoder(nn.Module):

    def __init__(self, hidden_size=200, latent_size=2):
        super(MLPDecoder, self).__init__()
        self.linear1 = nn.Linear(latent_size + 10, hidden_size)
        self.linear2 = nn.Linear(hidden_size, 784)

    def forward(self, z, c):
        # 重建图像Decoder: (z,c) -> (x_)（10/100)
        # P(y|x, z)

        x_ = None
        ##############################################
        # TODO: Implement Here
        tmp = torch.cat((z, c), dim=-1)
        x_ = self.linear1(tmp)
        x_ = self.linear2(x_)
        ##############################################

        return x_


# %%
class CVAE(nn.Module):

    def __init__(self, hidden_size=200, latent_size=5, encoder_type='mlp'):
        super(CVAE, self).__init__()
        self.hidden_size = hidden_size
        self.latent_size = latent_size
        if encoder_type == 'mlp':
            self.encoder = MLPEncoder(hidden_size, latent_size)
        else:
            self.encoder = ConvEncoder(hidden_size, latent_size)

        self.decoder = MLPDecoder(hidden_size, latent_size)
        self.conEncoder = ConditionEncoder(hidden_size, latent_size)

    def reparemete(self, mu, std):
        esp = torch.randn_like(std)
        return mu + esp * std

    def forward(self, x, c):
        means, std = self.encoder(x, c)  # P(z | x, y)
        means_, std_ = self.conEncoder(c)  # P(z | x)

        ##############################################
        # 实现重参数化过程 (15/100)
        # TODO: Implement Here

        z = self.reparemete(means, std)

        ##############################################

        return self.decoder(z, c), means, std, means_, std_

    def infer(self, c):

        ##############################################
        # 在infer的时候，如何得到隐变量的先验分布和进而重构x？(10/100)
        # TODO: Implement Here
        means_, std_ = self.conEncoder(c)
        # z = self.reparemete(means_, std_)
        z = torch.randn_like(std_)
        ##############################################

        return self.decoder(z, c)


# %%
class Loss(nn.Module):

    def __init__(self):
        super(Loss, self).__init__()
        self.loss = nn.MSELoss()
        self.lamda = 1e-5

    def forward(self, x_, x, means, std, means_, std_):
        x = torch.flatten(x, start_dim=1)
        mse_loss = self.loss(x_, x)

        ##############################################
        # 实现KL loss(20/100)
        # TODO: Implement Here
        # KL: https://stats.stackexchange.com/questions/7440/kl-divergence-between-two-univariate-gaussians

        # eps = 1e-16
        # kl_loss = torch.log(std / (std_ + eps))
        # kl_loss += (torch.square(std) + torch.square(means - means_)) / (
        #     2 * torch.square(std_) + eps)
        # kl_loss -= 0.5

        # kl_loss = kl_loss / torch.norm(kl_loss)
        # kl_loss = torch.mean(kl_loss)
        # if kl_loss < 0:
        #     print(kl_loss)
        #     exit(0)
        
        var = torch.pow(torch.exp(std), 2)
        var_ = torch.pow(torch.exp(std_), 2)
        
        d1 = torch.sum(1 + torch.log(var) - torch.pow(means, 2) - var)
        d2 = torch.sum(1 + torch.log(var) - torch.pow(means_, 2) - var_)
        kl_loss = d1 + d2


        ##############################################

        return mse_loss - self.lamda * kl_loss


# %%
# 可修改以下参数使得你的重构后的图片效果更好
dataset = FashionDataset('data/MNIST/train-images.idx3-ubyte',
                         'data/MNIST/train-labels.idx1-ubyte')

train_loader = torch.utils.data.DataLoader(dataset,
                                           batch_size=2048,
                                           shuffle=True)

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print(device)

model = CVAE(encoder_type='mlp')
model.to(device)

criterion = Loss()

epochs = 25
optimizer = torch.optim.Adam(model.parameters(), lr=3 * 1e-4)


# %% some helper function
def id2Onehot(y):
    if y.dim() == 1:
        y = y.unsqueeze(1)
    onehot = torch.zeros(y.size(0), 10)
    onehot.scatter_(1, y, 1)

    return onehot

def img_mask(imgs: torch.Tensor):
    prob = 0.9
    n, w, h = imgs.size()
    
    indices = torch.bernoulli(torch.zeros(n) + prob)
    indices = indices.type(torch.BoolTensor)
    mode = randint(0, 2)
    if mode == 0:
        imgs[indices, :w//2, :] = 0
    elif mode == 1:
        imgs[indices, :, :h//2] = 0
    elif mode == 2:
        imgs[indices, :w//2, :h//2] = 0

    return imgs


# %%
# 训练过程
def train(encoder_type='mlp', with_mask=False):
    for epoch in range(epochs):
        train_loss = []
        tq_iter = tqdm(train_loader)
        for step, batch in enumerate(tq_iter):
            x, y = batch

            ##########################################################
            # Random mask img and transfer shape of tensor
            x = img_mask(x) if with_mask else x
            x = x / 255.
            x = x.view(x.size(0), -1) if encoder_type == 'mlp' else x
            ##########################################################
            x = x.to(device)
            c = id2Onehot(y)
            c = c.to(device)
            x_, means, std, means_, std_ = model(x, c)
            loss = criterion(x_, x, means, std, means_, std_)
            train_loss.append(loss.cpu().item())
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
        print("epoch:%d loss:%.5f" % (epoch, np.mean(train_loss)))

        with torch.no_grad():

            if not os.path.exists('./figs/%s/epoch_%d' %
                                  (encoder_type, epoch)):
                print('hh')
                os.makedirs('./figs/%s/epoch_%d' % (encoder_type, epoch))

            ##############################################
            # infer时，保存每个epoch中 label对应的重构图像 (10/100)
            # TODO: Implement Here
            c = id2Onehot(torch.tensor([i for i in range(10)])).to(device)
            x_ = model.infer(c)

            plt.figure(figsize=(5, 10))
            for i in range(10):
                plt.subplot(5, 2, i + 1)
                plt.imshow(x_[i].view(28, 28).cpu().data.numpy())
                # print(x_[i])
                plt.axis('off')

            plt.savefig('./figs/%s/epoch_%d/%s.png' %
                        (encoder_type, epoch, encoder_type))
            plt.clf()
            plt.close('all')
            ##############################################


train(with_mask=False)

# %%
#  上面我们使用的是MLPencoder，由于输入是图像，可以很自然的使用卷积网络,
# 因此在这部分请实现ConvEncoder
class ConvEncoder(nn.Module):

    def __init__(self, hidden_size=200, latent_size=6):
        super(ConvEncoder, self).__init__()
        self.c1 = nn.Conv2d(1, 10, kernel_size=3, stride=1, padding=1)
        self.pool = nn.MaxPool2d(3, stride=1, padding=1)
        self.drop = nn.Dropout()
        self.l1 = nn.Linear(28 * 28 * 10 + 10, hidden_size * 70)
        self.l2 = nn.Linear(hidden_size * 70, hidden_size)

        self.relu = nn.ReLU()
        self.std_fc = nn.Linear(hidden_size, latent_size)
        self.mean_fc = nn.Linear(hidden_size, latent_size)

    def forward(self, x, c):
        mean, std = None, None
        ##############################################
        # 实现ConvEncoder 前向计算(10/100)
        # TODO: Implement Here
        x = x.unsqueeze(1)
        x = self.c1(x)
        x = self.pool(x)
        x = self.drop(x)
        x = x.view(x.size(0), -1)

        x = torch.cat((x, c), dim=-1)
        tmp = self.l1(x)
        tmp = self.relu(tmp)
        tmp = self.l2(tmp)

        mean = self.mean_fc(tmp)
        std = torch.abs(self.std_fc(tmp))
        ###############################################
        return mean, std


# %%
model = CVAE(encoder_type='conv')
model.to(device)

criterion = Loss()

epochs = 30
optimizer = torch.optim.Adam(model.parameters(), lr=3 * 1e-4)

# %%
train('conv', with_mask=True)


# %%
print('Hello')
# %%
