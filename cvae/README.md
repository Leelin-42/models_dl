# 实验报告以及说明

#### 文件内容说明

**本次实验应为个人习惯问题把 `.ipynb` 导出为了 `.py` 文件，主要的实现均在 `cvae.py` 文件中**

同时所有的个人实现的部分都使用了明显的标识隔离
```python
#####################################
# TOOD: tips
code
#####################################
```

#### 实验结果展示

##### 对 Img 未做随机化 Mask

- 基于 MLP 作为 Encoder 的结果
    ![](figs/mlp_origin/epoch_39/mlp.png)
    
- 基于 CNN 作为 Encoder 的结果
    ![](figs/conv_origin/epoch_37/conv.png)

##### 做随机化 Mask

- 基于 MLP 作为 Encoder 的结果
    ![](figs/mlp_maskprob0.9/epoch_24/mlp.png)
    
- 基于 CNN 作为 Encoder 的结果
    ![](figs/conv_maskprob0.9/epoch_29/conv.png)
    
对 Img 做的 Mask 操作即为随即的选取一部分原数据，对左右或者上下置 0

```python
def img_mask(imgs: torch.Tensor):
    prob = 0.9
    n, w, h = imgs.size()
    
    indices = torch.bernoulli(torch.zeros(n) + prob)
    indices = indices.type(torch.BoolTensor)
    mode = randint(0, 2)
    if mode == 0:
        imgs[indices, :w//2, :] = 0
    elif mode == 1:
        imgs[indices, :, :h//2] = 0
    elif mode == 2:
        imgs[indices, :w//2, :h//2] = 0
    return imgs
```

可以看到即使在 `prob=0.9` 的情况下 CVAE 的学习能力一蛮好的，在 `prob=0.1` 的设置下基本看不出区别

> 但是没有想到如何对于 CVAE 的效果以什么样的指标进行评估，此处就单纯的看一眼 inference 的效果

![](figs/conv_makprob0.1/epoch_22/conv.png)

#### 实验部分实现以及讨论

在实现 KL 散度的时候，值得注意的有几点：

1. 使用 `torch.log(x)` 的时候需要 `x` 非负，但是如果使用 `nn.ReLU()` 作为截断方法似乎对于分布 $P(z| x,y),\, P(z|x)$ 引入了严重的噪声，导致模型无法训练
2. KL 散度的计算需要和作为 $\mathbb{E}_{p_\phi(z|x,y)}[\log P_\Theta(y|x, z)]$ 的近似的 MSELoss 到相同的量级，不然会因为使用的是梯度方法导致两个部分的优化不一致